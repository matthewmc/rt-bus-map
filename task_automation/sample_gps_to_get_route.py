# Since the coordinates of the bus stops alone aren't enough to accurately plot
# the bus route and Google Roads API snapToRoads does not provide a good set of points
# as well with that set of points, a buffer of 100 (the max snapToRoads will 
# take) of realtime bus coordinates will be taken and fed to the Roads API.
# The results will then be appended to a final set to be written to a timestamped file.

import requests
import datetime
import time
import pickle
# from .. import consts

BASE_URL= 'http://api.metro.net/agencies/lametro/'
ROADS_API_KEY = 'AIzaSyBlVCl_ggT1Uo1zGD9XXJ4UzttQgyQm0gM'
MAPS_API_KEY = ''
ROADS_URL = 'https://roads.googleapis.com/v1/snapToRoads?path='

class LatLng():
    def __init__(self, lat, lng):
        self.lat = float(lat)
        self.lng = float(lng)






# Get all route numbers for LA Metro
endp = 'routes/'
res = requests.get(BASE_URL + endp)
lines_json = res.json()['items']
lines = [x['id'] for x in lines_json]

# Takes bus line number. Requests bus position for 
# all lines for roughly 300 cycles with approx. 1 minute of wait time between requests.
# Arbitrarily selecs one of the buses running.
# Writes to txt files with file names formatted LINE_NUM-YEAR-MONTH-DAY.
def scrape_and_write(num):
    
    # Format URLs.
    endp = 'routes/' + str(num) + '/vehicles/'
    stops_endp = 'routes/' + str(num) + '/stops/'
    url = BASE_URL + endp
    stops_url = BASE_URL + stops_endp

    # Format filename.
    d = datetime.date.today()
    name_ = '-'.join([str(num), str(d.year), str(d.month), str(d.day)])

    done = False
    buffer_ = []
    bus_id = None
    final = []
    curr = None
    pre_run_id = None
    curr_run_id = None

    # Get route endpoints.
    stops = requests.get(stops_url).json()['items']
    end1 = LatLng(stops[0]['latitude'], stops[0]['longitude']) 
    end2 = LatLng(stops[len(stops) - 1]['latitude'], \
                  stops[len(stops) - 1]['longitude']) 

    with open(name_ + '.txt', 'w') as f:
        while done == False:
#            try:
            res = requests.get(url)
            res = res.json()['items']

            if bus_id == None:
                # Try to select the first bus that's available.
                try: 
                    bus = res[0]
                    pre_run_id = bus['run_id']
                    bus_id = bus['id']

                except IndexError:
                    print('No buses right now.')

            if bus_id != None:
                for bus in res:
                        if bus['id'] == bus_id:

                            if curr == None:
                                curr = LatLng(bus['latitude'], bus['longitude'])
                            if curr_run_id == None:
                                curr_run_id = bus['run_id']

                            # Check if bus run has changed and stop scraping.  
                            if bus['run_id'] != curr_run_id:
                                get_snapped(buffer_, final)
                                print('run_id change happened.')
                                done = True                        
                            elif bus['run_id'] != pre_run_id:
                                new = LatLng(veh['latitude'], veh['longitude'])
                                if new.lat == curr.lat and new.lng == curr.lng:
                                    print(new.lat, new.lng, 'Same')
                                # If the new point is actually new, replace old.
                                else:
                                    curr = new
                                    if len(buffer_) < 8:
                                        buffer_.append(new)
                                        print('Working: ', [(x.lat, x.lng) for x in buffer_])
                                    else:
                                        buffer_.append(new)
                                        get_snapped(buffer_, final)
                    

                """
                    # If bus position near design. endpoint, end.
                    if dist_thres(curr, end):
                        done = True

            except Exception as e:
                print('Bad request.', e)
            """

            # Wait approx. 2 minutes between Metro API requests
            time.sleep(60)

        # Process dregs in buffer; write final to file. 
        get_snapped(buffer_, final)
        f.write('\n'.join([str(x.lat) + ',' + str(x.lng) for x in buffer_]))


def get_snapped(buffer_, final):
    try:
        if len(buffer_) > 1:
            # Construct the Google Roads API URL with the stops coordinates
            path = [str(x.latitude) + ',' + str(x.longitude) for x in buffer_]
            path = '|'.join(path)
            g_url = ROADS_URL + path + \
                    '&key=' + ROADS_API_KEY + '&interpolate=true'

            # Get the SnapToRoads result coordinates
            res = requests.get(g_url)
            seq_json = res.json()['snappedPoints']
            final.extend([LatLng(x['location']['latitude'], 
                                 x['location']['longitude']) 
                                 for x in seq_json])
            buffer_ = []
        else:
            # Do nothing to final if the buffer is empty 
            pass
    except:
        pass

def dist_thres(ll1, ll2, thres=0.5):
    pass


if __name__ == '__main__':
    scrape_and_write(720)