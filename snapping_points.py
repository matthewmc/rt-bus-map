from math import degrees, asin, sin, cos, inf, sqrt

# Takes latitude and longitude in degrees, (optional: 'rad' for radians), 
def haversine(point, point2, type_='deg'):
    
    try:
        lat = point[0]
        lng = point[1]
        lat2 = point2[0]
        lng2 = point2[1]

    except:
        pass
        # raise Exception

    # Test inputs
    possible = ('deg', 'rad')
    if type_ not in possible:
        pass
        # raise Exception of some sort

    # Convert from radians to degrees
    if type_ == 'rad':
        lat = degrees(lat)
        lng = degrees(lng)
        lat2 = degrees(lat2)
        lng2 = degrees(lng2)

    r = 6378.137  # km, Earth radius at Equator

    # Haversine formula
    dist = 2 * r * asin(
        sqrt(
            (sin(
                (lat2 - lat) / 2
                )
            ) ** 2 + 
        cos(lat) * cos(lat2) * (
            sin((lng2 - lng) / 2)
                            ) ** 2)
    )
    
    return dist


# Takes a list of valid tuples,  a metric function, and 
# a test point, returns the index of the tuple that's closest to the test point.
def get_closest(test_point, list_, metric):
    curr = -1
    min_ = inf
    for i in range(len(list_)):
        point = list_[i]
        try:
            dist = metric(test_point, point)
            if dist < min_:
                curr = i
                min_ = dist
        except:
            pass

    if curr == -1:
        pass 
        # raise Exception
    return curr

# Given a set of points representing a path and a specific test point, returns
# the indices of the two points in the path bounding the test point.
def get_bounds(test_point, points):
    bound1 = get_closest(test_point, points, haversine)

    # Case 1: the closest point found is at the beginning or end of path 
    if bound1 == 0:
        return [0, 1]
    elif bound1 == len(test_point) - 1:
        end = len(test_point) - 1
        return [end - 1, end]

    # Case 2: test the closest point's neighbors against the test point
    else:
        # Candidates
        cd1 = bound1 - 1
        cd2 - bound1 + 1
        if haversine(cd1, test_point) <= haversine(cd2, test_point):
            return [cd1, bound1]
        else:
            return [bound1, cd2]
