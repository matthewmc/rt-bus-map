# README #

* Quick summary
> A real-time LA Metro bus map 

* Current features:
    - Displays a map of bus locations from the LA Metro API
    - Bus markers change color as the age of the data increases.
    - JS widget to draw and get a JSON object of a bus' route

>  Note: limitations on some of these features apply.

* Version
0.1


### How do I get set up? ###

* Summary of set up
>  Clone the repo.  pip install dependencies.  Add a Google Maps API key and a Google Roads API key to settings.py.  Run flask-site.py.

* Configuration

* Dependencies
    - Flask
    - flask-limiter
    - requests

* Database configuration

* How to run tests

* Deployment instructions


### Contribution guidelines ###

* To do: 
    - Commentary coverage
    - Testing coverage
    - Users
    - Automated route scraping
    - Moving vehicle prediction markers

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact