import requests

BASE_URL= 'http://api.metro.net/agencies/lametro/'
ROADS_API_KEY = ''
MAPS_API_KEY = ''
ROADS_URL = 'https://roads.googleapis.com/v1/snapToRoads?path='
ROUTES_DIR = 'static/route-Polylines/'
DB_DIR = 'vehicles.db'


class LatLng():
    def __init__(self, lat, lng):
        self.lat = float(lat)
        self.lng = float(lng)

# Takes url parameter, cleans up the JSONObject before handing it back.
def get(url):
    try:
        res = requests.get(url)
        res = res.json()['items']
        return res
    except:
        pass

def chunk_list(list_, size=100):
    """
    Takes a list of elements, outputs a list of chunks
    each consisting of a list with optional max size param
    derived from the original list of elements.

    Args: 
        list_: list of elements.
        [int] size (optional): Defaults to 100.  Max size of the 
            output.

    Returns:
        [list]: list of chunks, which themselves are lists.

    """
    final = []
    while len(list_) > 0:
        final.append(list_[0:size])
        del list_[0:size]
    return final

def concat_chunks(list_):
    """
    Takes a list of chunks and outputs a list one level 
    flatter.  

    Args:
        [iterable] list_: list of chunks

    Returns:
        [list]: list of the same elements, one level of list
            nesting removed.   
    """
    final = []
    for chunk in list_:
        final.extend(chunk)
    return final