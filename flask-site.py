from flask import Flask, render_template, url_for, send_from_directory
from settings import *
from os import path
from json import dumps, loads
import requests
from flask_limiter import Limiter 
import sqlite3 as sql
from time import time


app = Flask(__name__, static_url_path='')
limiter = Limiter(app, global_limits=['60 per hour'])

@app.route('/')
def get_lines():
    url = BASE_URL + 'routes/'
    lines_json = get(url)
    return render_template('select_bus.html', lines_json=lines_json)

@app.route('/<int:line_num>')
def get_specific_line(line_num):
    # Take the bus route number parameter to request the degree coordinates of the 
    # specific stops from the LA Metro API
    url = BASE_URL + 'routes/' + str(line_num) + '/sequence/'
    route_dirpath = ROUTES_DIR + str(line_num) + '.txt'
    stops_json = get(url)

    seq_json = []
    with open(route_dirpath) as f:
        f = loads(f.read())
        seq_json= [{'lat' : float(x['lat']), 'lng' : float(x['lng'])} for x in f]
    return render_template('specific_bus.html', 
                            stops_json=stops_json, 
                            seq_json=seq_json, 
                            MAPS_API_KEY=MAPS_API_KEY,
                            line_num=line_num)

@app.route('/route_maker')
def make_route():
    return render_template('route_maker.html')

@app.route('/api/<int:line_num>/vehicles')
def get_vehicles(line_num):

    """

    class NoVehiclesError(Exception):
        pass

    def check_new(veh):
        res = curs.execute('SELECT * FROM vehicles WHERE route = {route} \
                                                         run_id = {run_id} \
                                                         lat = {lat} \
                                                         lng = {lng}'
                                                        .format(**veh))
        if len(res) == 1:
            return True
        else:
            return False

    try:
        vehs = curs.execute('SELECT * FROM vehicles WHERE route = {0}'
                                                           .format(line_num))
        if len(vehs) == 0:
            raise(NoVehiclesError)
        else:
            for veh in vehs:
                veh = list(veh)

    except NoVehiclesError:
        url = BASE_URL + 'routes/' + str(line_num) + '/vehicles/'
        req = requests.get(url).json()
        req = req['items']
        for veh in req:
            if check_new(veh) == True:
                curs.execute('DELETE * FROM vehicles WHERE run_id = {0}  \
                                                        AND route = {1};'
                                                        .format(veh[run_id], 
                                                                line_num))
                curs.execute('INSERT INTO vehicles VALUES ({route}, \
                                                           {run_id}, \
                                                           {lat}, \
                                                           {lng}, \
                                                           {incept}'
                                                           .format(**veh, incept=time()))
    """    
    url = BASE_URL + 'routes/' + str(line_num) + '/vehicles/'
    req = requests.get(url).json()
    req = req['items']
    return dumps(req)

@app.route('/images/<image_file>')
def get_image(image_file):
    try:
        return send_from_directory('static/images', image_file)
    except:
        print('failed')

@app.route('/static/<js_file>')
def get_js(js_file):
    return send_from_directory('static/', js_file)

def build_db():

    """
    con = sql.connect(DB_DIR)
    curs = con.cursor() 
    curs.execute('DROP vehicles')
    curs.execute('CREATE TABLE vehicles (i INTEGER PRIMARY KEY, route, run_id, lat, lng, incept);')
    """
    pass


if __name__ == '__main__':
    build_db()
    app.run(debug=True)