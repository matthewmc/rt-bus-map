var map;
var line; 

function initMap(){
    options = {
        zoom: 3,
        center: {lat: 0, lng: -180}
    };

    poly_opts = {
        strokeWeight: 2,
        strokeOpacity: 1.0
    };

    map = new google.maps.Map(document.getElementById('map'),
        options);

    line = new google.maps.Polyline(
        poly_opts);
    line.setMap(map);

    map.addListener('click', addToPath);
    map.addListener('rightclick', removeFromPath);
    var save = document.getElementById('save');
    save.addEventListener('click', saveToFile);
};

// Add left click event to add to Polyline.
function addToPath(event){
    var path = line.getPath();
    path.push(event.latLng);
};

// Add right click event to remove from Polyline.
function removeFromPath(event){
    var path = line.getPath();
    path.pop();
};

// Output JSON string of latLngs for export.  
// See if this can be implemented better with file writing 
// and/or automatic passing to Google Roads API to snapToRoad().
function saveToFile(){
    var path = line.getPath();
    var output = document.getElementById('output');
    output.textContent = JSON.stringify(path['j']);
};
