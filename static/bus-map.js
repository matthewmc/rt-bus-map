function initMap(){
    var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 3,
    center: {lat: 0, lng: 0},
    mapTypeId: google.maps.MapTypeId.TERRAIN
    });

    var path = [];
    var bounds = new google.maps.LatLngBounds();

    // Set bus stop icon image and size.
    var image = {
        url: 'images/placeholder.png',
        scaledSize: new google.maps.Size(20, 30),
    };

    // Populate map with bus stops represented by custom markers
    var stops = [];
    for (i = 0; i < stops_json.length; i++){
        curr = stops_json[i];
        stop = new google.maps.Marker({
            position: {lat: curr['latitude'], lng: curr['longitude']},
            draggable: false,
            map: map,
            icon: image
        });
        stops.push(stop);
    }

    //  Read in route path from JSON.
    for (i = 0; i < seq_json.length; i++){
        curr = seq_json[i];
        curr_latlng = new google.maps.LatLng(
            curr['lat'], curr['lng']);
        path.push(curr_latlng);

        // Add all path coords to later center and zoom on Polyline.
        bounds.extend(curr_latlng);
    }

    var route = new google.maps.Polyline({
        path: path,
        geodesic: true
    });

    route.setMap(map);
    map.fitBounds(bounds);
    icons = {};
    animateMap(route, map);
}

function augOpts(lat, lng){
    this.icon = new google.maps.LatLng(lat, lng);
    this.incept = Date.now()
    this.color_ = color_;
    this.check_new = check_new;
    this.set_marker = set_marker;
    this.curr_time = curr_time;
    this.update_marker = update_marker;
    this.del_marker = del_marker;

    // Returns the age of the object in seconds.
    function curr_time(){
        return (Date.now() - this.incept) / 1000;
    }

    function color_(){
        function toHex(num){
            return Math.round(num).toString(16);
        }

        var tot = 20;
        var r, g, b;
        var ct = this.curr_time();
        var bd = [1/4, 1/2, 3/4, 1].map(function(x){return x * tot});

        if (ct >= 0 && ct < bd[0]){
            r = 255;
            g = 134 + (ct/bd[0]) * 76;
            b = 99 + (ct/bd[0]) * 97;
        }
        else if (ct >= bd[0] && ct < bd[1]){
            sc = (ct - bd[0])/bd[0];
            r = 255 - 33 * sc;
            g = 210 + 12 * sc;
            b = 196 + 26 * sc;
        }
        else if (ct >= bd[1] && ct < bd[2]){
            sc = (ct - bd[1])/bd[0];
            r = 222 - 26 * sc;
            g = 222 + 19 * sc;
            b = 222 + 33 * sc;
        }
        else if (ct >= bd[2] && ct < bd[3]){
            sc = (ct - bd[2])/bd[0];
            r = 196 - 97 * sc;
            g = 241 - 21 * sc;
            b = 255; 

        }   
        else{
            r = 99;
            g = 220;
            b = 255;
        }

        return toHex(r) + toHex(g) + toHex(b);
    }

    function check_new(lat, lng){
        if (lat == this.icon.lat() && lng == this.icon.lng()){
            return true;
        } 
        else{
            return false;
        }
    }

    // Instantiates 
    function set_marker(){
        symbOpts = {
            path: google.maps.SymbolPath.CIRCLE,
            scale: 7,
            strokeColor: '#' + this.color_()
        };

        markOpts = {
            position: this.icon,
            icon: symbOpts
        };
        this.marker = new google.maps.Marker(markOpts);
        return this.marker;
    }

    function update_marker(){
        symbOpts = {
            path: google.maps.SymbolPath.CIRCLE,
            scale: 7,
            strokeColor: '#' + this.color_()
        };
        this.marker.setIcon(symbOpts);
    }

    function del_marker(){
        this.marker.setMap(null);
        this.marker = null;
    }

}

function animateMap(line, map)
{
    count = 0;
    url = 'http://127.0.0.1:5000/api/' + line_num + '/vehicles';
    var icons = {};
    var new_ = false;

    function loop()
    {
        if (count >= 400 || Object.keys(icons).length == 0)
        {
            count = 0;
            var vehicles = new XMLHttpRequest();
            vehicles.open('GET', url, true);
            vehicles.onreadystatechange = function()
            {
                if (vehicles.readyState == 4 && vehicles.status == 200)
                {
                    new_ = true;
                    vehicles = JSON.parse(vehicles.responseText);
                    var prev_icons = icons; 
                    icons = {};
                    for (i in vehicles)
                    {
                        id = vehicles[i]['id'];
                        lat = vehicles[i]['latitude'];
                        lng = vehicles[i]['longitude'];
                        if (id in icons)
                        {
                            if (prev_icons[id].check_new(lat, lng) == true)
                            {
                                prev_icons[id].del_marker();
                                icons[id] = new augOpts(lat, lng);
                            }
                        }
                        else
                        {
                            icons[id] = new augOpts(lat, lng); 
                        }
                    }
 
                    for (var key in Object.keys(prev_icons))
                    {
                        if (prev_icons.hasOwnProperty(key))
                        {
                            if (prev_icons[key].curr_time() <= 300)
                            {
                                icons[key] = prev_icons[key];
                            }
                            else
                            {
                                prev_icons[key].del_marker();
                            }
                        }
                    }
                }
            }
            vehicles.send();
            count += 1;   
        }

        if (new_ == true)
        {
            new_ = false;
            for (i in icons)
            {
                curr = icons[i].set_marker();
                curr.setMap(map);
            }
        }
        else
        {
            for (i in icons)
            {
                icons[i].update_marker();
            }
        }
    }
    window.setInterval(loop, 25);
}

